#include "Integer.h"

Integer::Integer(int val)
{
	this->_data = val;
}

bool Integer::isPrintable()
{
	return true;
}

std::string Integer::toString()
{
	return std::to_string(this->_data);
}
