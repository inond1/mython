#ifndef VOID_H
#define VOID_H

#include "type.h"

class Void : public Type
{
private:
public:
	bool isPrintable();
	std::string toString();
};



#endif // VOID_H