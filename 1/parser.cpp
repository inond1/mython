#include "parser.h"
#include <iostream>
std::unordered_map<std::string, Type*> Parser::_variables;
Type* Parser::parseString(std::string str) throw()
{
	/*if (str.length() > 0)
	{
		if (str[0] == ' ' || str[0] == '\t')
		{
			throw IndentationException();
		}
		std::cout << str << std::endl;
	}*/
	Helper::rtrim(str);

	Type* ret = getVariableValue(str);
	if (ret != NULL) return ret;
	ret = getType(str);
	if (ret != NULL) return ret;
	
	if (makeAssignment(str))
	{
		Type* ret = new Void();
		ret->setIsTemp(true);
		return ret;
	}
	throw SyntaxException();

}

Type* Parser::getType(std::string& str)
{
	if (Helper::isInteger(str))
	{
		Helper::removeLeadingZeros(str);
		Type* type  = new Integer(std::stoi(str));
		type->setIsTemp(true);
		return type;
	}
	else if (Helper::isBoolean(str))
	{
		Type* type;
		if (str == "False")
			type = new Boolean(false);
		else
			type = new Boolean(true);
		type->setIsTemp(true);
		return type;
	}
	else if (Helper::isString(str))
	{
		if (str[0] == '\"') {
			str[0] = '\'';
			str[str.length() - 1] = '\'';
		}
		Type* type = new String(str);
		type->setIsTemp(true);
		return type;
	}
	return NULL;
}

void Parser::clean()
{
	_variables.clear();
}

bool Parser::isLegalVarName(const std::string& str)
{
	if (Helper::isDigit(str[0])) return false;
	if (!Helper::isLetter(str[0]) && str[0] != '_') return false;

	for (size_t i = 1; i < str.length(); i++)
	{
		if (!Helper::isDigit(str[i]) && !Helper::isLetter(str[i]) && str[i] != '_')
			return false;
	}
	return true;
}

bool Parser::makeAssignment(const std::string& str)
{
	size_t pos = str.find('='); //finding =
	if (pos == std::string::npos) return false;
	std::string value = str.substr(pos + 1); //value
	std::string variable_name = str; // name
	variable_name.erase(pos);
	if (!isLegalVarName(variable_name)) throw NameErrorException(variable_name); //checking if legal
	Type* variable = getVariableValue(variable_name);
	if (variable != NULL) return variable;
	variable = getType(value);
	if (variable == NULL)
	{
		if (_variables.find(value) != _variables.end())
		{
			variable = _variables[value];
		}
	}
	if (_variables.find(variable_name) != _variables.end())
		_variables[variable_name] = variable;
	else
		_variables.insert(std::pair<std::string, Type*>(variable_name, variable));
	return true;
}

Type* Parser::getVariableValue(const std::string& str)
{
	if (_variables.find(str) != _variables.end())
	{
		return _variables[str];
	}
	return NULL;
}


