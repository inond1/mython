#include "String.h"

String::String(std::string val)
{
	this->_data = val;
}

bool String::isPrintable()
{
	return true;
}

std::string String::toString()
{
	return this->_data;
}
