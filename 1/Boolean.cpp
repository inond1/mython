#include "Boolean.h"

Boolean::Boolean(bool val)
{
	this->_data = val;
}

bool Boolean::isPrintable()
{
	return true;
}

std::string Boolean::toString()
{
	return (this->_data ? "True" : "False");
}
