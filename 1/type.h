#ifndef TYPE_H
#define TYPE_H

#include <string>

class Type
{
private:
	bool _isTemp = false;

public:
	bool getIsTemp() const;
	void setIsTemp(bool boolean);
	virtual bool isPrintable() = 0;
	virtual std::string toString() = 0;
};





#endif //TYPE_H
