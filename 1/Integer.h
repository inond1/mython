#ifndef INTEGER_H
#define INTEGER_H

#include "type.h"

class Integer : public Type
{
private:
	int _data;

public:
	Integer(int val);
	bool isPrintable();
	std::string toString();
};


#endif // INTEGER_H