#ifndef STRING_H
#define STRING_H

#include "Sequence.h"

class String : public Sequence
{
private:
	std::string _data;
public:
	String(std::string val);
	bool isPrintable();
	std::string toString();
};


#endif // STRING_H