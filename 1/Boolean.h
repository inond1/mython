#ifndef BOOLEAN_H
#define BOOLEAN_H

#include "type.h"

class Boolean : public Type
{
private:
	bool _data;
public:
	Boolean(bool val);
	bool isPrintable();
	std::string toString();
};


#endif // BOOLEAN_H